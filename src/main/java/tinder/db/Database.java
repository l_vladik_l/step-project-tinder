package tinder.db;

import lombok.SneakyThrows;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;

public class Database {
    
    public static void checkAndApplyDeltas(){
        FluentConfiguration conf = new FluentConfiguration().dataSource(ConnectionDetails.url, ConnectionDetails.username, ConnectionDetails.password);
        Flyway flyway = new Flyway(conf);
//        flyway.migrate();

    }
    @SneakyThrows
    public static Connection connection(){
        return DriverManager.getConnection(ConnectionDetails.url, ConnectionDetails.username, ConnectionDetails.password);
    }
}
