package tinder.dao;

public interface Identifable {
    Integer id();
}
